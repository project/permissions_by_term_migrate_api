CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Permissions By Term Migrate API extends the Permissions By Term
module so that it can be included as a destination for a migration.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/permissions_by_term_migrate_api

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/permissions_by_term_migrate_api

REQUIREMENTS
------------

This module requires permissions_by_term module and migrate API from Core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To use this module - you must:
* Include the permissions_by_term information in a migration.

MAINTAINERS
-----------
Current maintainers:
 * Damyon Wiese - https://www.drupal.org/user/3616877

This project has been sponsored by:
 * Doghouse Agency - specializing in developing drupal solutions . Visit https://doghouse.agency/ for more information
