<?php

namespace Drupal\permissions_by_term_migrate_api\Service;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\permissions_by_term\Cache\KeyValueCache;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\permissions_by_term\Service\AccessStorage;

/**
 * Class AccessStorage.
 *
 * @package Drupal\permissions_by_term_migrate_api
 */
class AccessStorageMigrate extends AccessStorage {

  /**
   * @param FormState $formState
   * @param int $term_id
   *
   * @return array
   * @throws \Exception
   */
  public function saveTermPermissions(FormStateInterface $formState, $term_id) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (!empty($formState->getValue('langcode'))) {
      $langcode = $formState->getValue('langcode')['0']['value'];
    }

    $aSubmittedUserIdsGrantedAccess = $this->getSubmittedUserIds($formState);
    $aSubmittedRolesGrantedAccess = $this->getSubmittedRolesGrantedAccess($formState);

    return $this->saveTermPermissionsByIds($aSubmittedUserIdsGrantedAccess, $aSubmittedRolesGrantedAccess, $term_id, $langcode);
  }

  /**
   * Modify the list of users and roles who can access a term.
   *
   * @param int[] $users
   *   - The list of user ids to allow access to the term.
   * @param int[] $roles
   *   - The list of role ids to allow access to the term.
   * @param int $term_id
   *   - The term to modify access permissions.
   * @param string $langcode
   *   - The lang code.
   *
   * @return array
   *   - The list of users and roles that were modified.
   *
   * @throws \Exception.
   *   - Any lower level exception.
   */
  public function saveTermPermissionsByIds(array $users, array $roles, $term_id, $langcode) {
    if (empty($langcode)) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    $aExistingUserPermissions       = $this->getUserTermPermissionsByTid($term_id, $langcode);
    $aSubmittedUserIdsGrantedAccess = $users;

    $aExistingRoleIdsGrantedAccess = $this->getRoleTermPermissionsByTid($term_id, $langcode);
    $aSubmittedRolesGrantedAccess  = $roles;

    $aRet = $this->getPreparedDataForDatabaseQueries($aExistingUserPermissions,
      $aSubmittedUserIdsGrantedAccess, $aExistingRoleIdsGrantedAccess,
      $aSubmittedRolesGrantedAccess);

    $this->deleteTermPermissionsByUserIds($aRet['UserIdPermissionsToRemove'], $term_id, $langcode);
    $this->addTermPermissionsByUserIds($aRet['UserIdPermissionsToAdd'], $term_id, $langcode);

    $this->deleteTermPermissionsByRoleIds($aRet['UserRolePermissionsToRemove'], $term_id, $langcode);
    if (!empty($aRet['aRoleIdPermissionsToAdd'])) {
      $this->addTermPermissionsByRoleIds($aRet['aRoleIdPermissionsToAdd'], $term_id, $langcode);
    }

    return $aRet;
  }

}
