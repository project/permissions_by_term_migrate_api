<?php

namespace Drupal\permissions_by_term_migrate_api\Plugin\migrate\destination;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\permissions_by_term_migrate_api\Service\AccessStorageMigrate;
use Drupal\permissions_by_term\Service\TermHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Permissions By Term Access destination plugin.
 *
 * Examples:
 *
 * @code
 * process:
 *
 *   tid:
 *    -
 *      plugin: migration_lookup
 *      migration: terms
 *      source: name
 *    -
 *      plugin: skip_on_empty
 *      method: row
 *      message: 'Field name could not map to a term'
 *  roles: roles
 *  users: users
 *
 * destination:
 *   plugin: permissions_by_term_access
 * @endcode
 *
 * @MigrateDestination(
 *   id = "permissions_by_term_access",
 *   requirements_met = true
 * )
 */
class PermissionsByTermAccess extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * Access storage for permissions.
   *
   * @var \Drupal\permissions_by_term_migrate_api\Service\AccessStorageMigrate
   */
  protected $accessStorage;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Term Handler.
   *
   * @var \Drupal\permissions_by_term\Service\TermHandler
   */
  protected $termHandler;

  /**
   * Constructs a content entity.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration entity.
   * @param \Drupal\permissions_by_term_migrate_api\Service\AccessStorageMigrate $access_storage
   *   The storage for this permission.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\permissions_by_term\Service\TermHandler $term_handler
   *   The term handler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, AccessStorageMigrate $access_storage, EntityFieldManagerInterface $entity_field_manager, TermHandler $term_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->accessStorage = $access_storage;
    $this->entityFieldManager = $entity_field_manager;
    $this->termHandler = $term_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('permissions_by_term_migrate_api.access_storage_migrate'),
      $container->get('entity_field.manager'),
      $container->get('permissions_by_term.term_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['value']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return ['value' => 'Term ID'];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $destination_values = $row->getDestination();
    if (isset($destination_values['users']) &&
        isset($destination_values['roles']) &&
        isset($destination_values['tid'])) {

      $this->accessStorage->saveTermPermissionsByIds(
        $destination_values['users'],
        $destination_values['roles'],
        $destination_values['tid'],
        ''
      );
    }
    return ['value' => $destination_values['tid']];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    // Delete the specified entity from Drupal if it exists.
    $this->accessStorage->deleteAllTermPermissionsByTid(reset($destination_identifier));
  }

}
